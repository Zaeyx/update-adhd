#!/bin/bash

#echo "Updating Artillery"
#cd /var/artillery
#git pull | tee -a /var/log/update

echo
echo "Updating Usage Documentation"
cd /opt/adhd_tools/adhd_docs
git pull | tee -a /var/log/update

echo
echo "Updating Jar-combiner"
cd /opt/jar-combiner
git pull | tee -a /var/log/update

echo 
echo "Updating Beartrap"
cd /opt/beartrap
git pull | tee -a /var/log/update

echo
echo "Updating BeEF"
cd /opt/beef
git pull | tee -a /var/log/update
bundle install

echo
echo "Updating easy-creds"
cd /opt/easy-creds
git pull | tee -a /var/log/update

echo
echo "Updating Honey-badger"
cd /opt/honey-badger
git pull | tee -a /var/log/update
echo "The update is complete, to commit, move the source from /opt/honeybadger to /var/www/honeybadger"
echo "Be mindful of any data you have previously captured."

#echo
#echo "Updating Honeyports"
#cd /opt/honeyports/cross-platform/honeyports
#git pull | tee -a /var/log/update

#echo
#echo "Updating Kippo"
#cd /opt/kippo
#svn update | tee -a /var/log/update

echo
echo "Updating Metasploit"
cd /opt/metasploit
git pull | tee -a /var/log/update
bundle install

echo
echo "Updating Spidertrap"
cd /opt/spidertrap
git pull | tee -a /var/log/update

echo
echo "Updating Tcprooter"
cd /opt/tcprooter
git pull | tee -a /var/log/update

echo
echo "Updating W3af"
cd /opt/w3af
git pull | tee -a /var/log/update

echo
echo "Updating Webbugserver"
cd /opt/webbugserver
git pull | tee -a /var/log/update

echo
echo "Updating Weblabyrinth"
cd /opt/weblabyrinth
git pull | tee -a /var/log/update

echo
echo "Updating system"
sudo apt-get update | tee -a /var/log/update
sudo apt-get upgrade -y | tee -a /var/log/update

#echo
#echo "Updating Nmap"
#cd /home/adhd/Desktop
#./.update-nmap
echo
echo "Should be done..."
echo "Logs are located at: /var/log/update"
